# Ansible Role: Hugo Extended

[![pipeline status](https://gitlab.com/enmanuelmoreira/ansible-role-hugo-extended/badges/main/pipeline.svg)](https://gitlab.com/enmanuelmoreira/ansible-role-hugo-extended/-/commits/main) 

This role installs [Hugo Extended Edition](https://github.com/gohugoio/hugo) binary on any supported host.

## Requirements

github3.py >= 1.0.0a3

It can be installed from pip:

```bash
pip install github3.py
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    hugo_version: latest # v0.92.0 if you want a specific version
    hugo_package_name: hugo
    setup_dir: /tmp

    hugo_bin_path: /usr/local/bin/hugo
    hugo_repo_path: https://github.com/gohugoio/hugo/releases/download

This role can install the latest or a specific version. See [available hugo releases](https://github.com/gohugoio/hugo) and change this variable accordingly.

    hugo_version: latest # v0.92.0 if you want a specific version

The path of the repo which hugo extended will be downloaded.

    hugo_repo_path: https://github.com/gohugoio/hugo/download

The path to the hugo home directory.

    hugo_bin_path: /usr/local/bin/hugo

Hugo only supports x86_64 CPU architecture.

Hugo needs a GitHub token so it can be downloaded. You must use the **github_token** variable in `vars/main.yml` in order to install Hugo. However, this value can be empty until GitHub reaches the maximum number of anonymous connections.

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: hugo-extended

## License

MIT / BSD
